﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleATC_Csharp
{
    class AirportImpl : IAirport
    {
        public string AirportId { get; private set; }
        public string AirportName { get; private set; }
        public ISet<IVor> VorSet { get; private set; }
        public IRadarPosition ParkingPosition { get; private set; }

        public AirportImpl(string airportId, string airportName, IRadarPosition parkingPosition, ISet<IVor> vorSet)
        {
            this.AirportId = airportId;
            this.AirportName = airportName;
            this.VorSet = VorSet;
            this.ParkingPosition = parkingPosition;
            if(vorSet != null)
            {
                this.VorSet = vorSet;
            }
        }

        public void AddVor(IVor newVor)
        {
            if(newVor != null)
            {
                this.VorSet.Add(newVor);
            }
            else
            {
                throw new ArgumentException();
            }
        }

        public IVor GetVorById(string vorId)
        {
            var foundVor = VorSet.Where(x => x.VorId == vorId).FirstOrDefault();
            if (foundVor != null)
            {
                return foundVor;
            }
            else
            {
                throw new KeyNotFoundException("Il vor non è stato trovato");
            }
            

        }

        public override string ToString()
        {
            return "\nAirport name: " + this.AirportName + "\n\tId: " + this.AirportId + "\n\t" + string.Join("", this.VorSet);
        }
    }
}
