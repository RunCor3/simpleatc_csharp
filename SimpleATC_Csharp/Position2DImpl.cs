using System;

namespace SimpleATC_Csharp
{
    class Position2DImpl : IPosition2D
    {
        public double X { get; private set; }
        public double Y { get; private set; }

        public Position2DImpl(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }

        public void AddX(double xOffset)
        {
            this.X = this.X + xOffset;
        }

        public void AddY(double yOffset)
        {
            this.Y = this.Y + yOffset;
        }

        public override string ToString()
        {
            return "x: " + this.X + " y: " + this.Y + " ";
        }
    }
}