﻿using System;
using System.Collections.Generic;

namespace SimpleATC_Csharp
{
    interface IAirport
    {
        string AirportId { get; }

        string AirportName { get; }

        IRadarPosition ParkingPosition { get; }

        ISet<IVor> VorSet { get; }

        void AddVor(IVor newVor);

        IVor GetVorById(string vorId);

    }
}
