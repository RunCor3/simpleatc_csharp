﻿using System;

namespace SimpleATC_Csharp
{
    interface IRadarElement
    {
        IRadarPosition ElementPosition { get; set; }
    }
}
