﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleATC_Csharp
{
    class AbstractRadarElement : IRadarElement
    {
        public IRadarPosition ElementPosition { get; set; }

        public AbstractRadarElement(IRadarPosition position)
        {
            this.ElementPosition = position;
        }
    }
}
