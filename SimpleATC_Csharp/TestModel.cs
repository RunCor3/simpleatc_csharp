using System;
using System.Collections.Generic;

namespace SimpleATC_Csharp
{
    class TestModel
    {
        public static void Main(string[] args)
        {
            //POSITION2D INITIALIZATION
            IPosition2D pos1 = new Position2DImpl(10.0, 0.0);
            IPosition2D pos2 = new Position2DImpl(10.0, 20.0);
            IPosition2D pos3 = new Position2DImpl(100.0, 1200.0);
            IPosition2D posOffBounds = new Position2DImpl(50000.0, 30000.0);
            IPosition2D pos4 = new Position2DImpl(20.0, 2.0);
            IPosition2D pos5 = new Position2DImpl(20.0, 20.0);
            Console.WriteLine("Position's Cordinates initialization:");
            Console.WriteLine("\t" + pos1.ToString());
            Console.WriteLine("\t" + pos2.ToString());
            Console.WriteLine("\n\n");

            //RADAR POSITION INITIALIZATION
            Console.WriteLine("RadarPosition's initialization:");
            IRadarPosition rdrPos1 = new RadarPositionImpl(pos1);
            IRadarPosition rdrPos2 = new RadarPositionImpl(pos2);
            IRadarPosition rdrPos3 = new RadarPositionImpl(pos3);
            IRadarPosition rdrPosOffBounds = new RadarPositionImpl(posOffBounds);
            IRadarPosition rdrPos4 = new RadarPositionImpl(pos4);
            IRadarPosition parkingPos = new RadarPositionImpl(pos5);
            Console.WriteLine("\t" + rdrPos1.ToString());
            Console.WriteLine("\t" + rdrPos2.ToString());
            Console.WriteLine("\t" + rdrPos3.ToString());
            Console.WriteLine("\n\n");

            //RADAR POSITION TEST, DISTANCE FROM TWO POSITIONS
            Console.WriteLine("Test of the distance between two RadarPosition's:");
            double distancePos1Pos2 = rdrPos1.DistanceFrom(rdrPos2);
            double distancePos1Pos3 = rdrPos1.DistanceFrom(rdrPos3);
            Console.WriteLine("\tDistance between pos1 and pos2: " + distancePos1Pos2);
            Console.WriteLine("\tDistance between pos1 and pos3: " + distancePos1Pos3);
            Console.WriteLine("\n\n");

            //RADAR POSITION TEST, ANGLE TO TARGET POSITION
            Console.WriteLine("Test of the angle between two points:");
            IDirection directionToPos2 = rdrPos1.ComputeDirectionToTargetPosition(rdrPos2);
            Console.WriteLine("\tDirection to pos2: " + directionToPos2);
            Console.WriteLine("\n\n");

            //RADAR POSITION TEST INSIDE-OUTSIDE RADAR
            Console.WriteLine("Test if a point is within radar or not:");
            Boolean withinRadar = rdrPos1.IsWithinRadar();
            Console.WriteLine("\tThis position " + rdrPos1 + " is within radar: " + withinRadar);

            withinRadar = rdrPosOffBounds.IsWithinRadar();
            Console.WriteLine("\tThis position " + rdrPosOffBounds + " is outside the radar: " + withinRadar);
            Console.WriteLine("\n\n");

            //VOR CREATION
            Console.WriteLine("Vor initialization:");
            IVor vor1 = new VorImpl("alfa", rdrPos1);
            IVor vor2 = new VorImpl("bravo", rdrPos2);
            IVor vor3 = new VorImpl("charlie", rdrPos3);
            Console.WriteLine("\t" + vor1);
            Console.WriteLine("\t" + vor2);
            Console.WriteLine("\n\n");

            //AIRPORT TEST
            Console.WriteLine("Airport initialization:");
            ISet<IVor> vorSet = new HashSet<IVor>();
            vorSet.Add(vor1);
            vorSet.Add(vor2);
            vorSet.Add(vor3);
            IAirport airport = new AirportImpl("LIRF", "Fiumicino", parkingPos, vorSet);

            Console.WriteLine("\t" + airport);

            try
            {
                IVor foundVor = airport.GetVorById("alfa");
                Console.WriteLine("\t Vor found: " + foundVor);
            }
            catch(KeyNotFoundException ex)
            {
                Console.WriteLine("\tException: " + ex);
            }

            try
            {
                IVor foundVor = airport.GetVorById("foxtrot");
                Console.WriteLine("\t" + foundVor);
            }
            catch (KeyNotFoundException ex)
            {
                Console.WriteLine("\tException thrown, vor with this id was not found");
            }
        }
    }
}