﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleATC_Csharp
{
    class DirectionImpl : IDirection
    {
        private const double MAX_ANGLE = 360;
        private const double MAX_DIFF = 180;
        public double Direction { get; private set; }

        public DirectionImpl(double direction)
        {
            this.Direction = direction;
        }

        private void CheckIfValid(double direction)
        {
            if (direction < 0 || direction > MAX_ANGLE)
            {
                throw new ArgumentException();
            }
        }

        public void Sum(IDirection direction)
        {
            this.Direction = (this.Direction + direction.Direction) % MAX_ANGLE;
        }

        public void Subtract(IDirection direction)
        {
            this.Direction = (this.Direction - direction.Direction < 0)
                    ? MAX_ANGLE + this.Direction - direction.Direction
                    : this.Direction - direction.Direction;
        }

        public double GetAsRadians()
        {
            return (Math.PI / MAX_DIFF) * this.Direction;
        }

        public double CompareTo(IDirection direction)
        {
            double rawDiff = (this.Direction - direction.Direction + MAX_DIFF) % MAX_ANGLE - MAX_DIFF;
            return Math.Abs(rawDiff < -MAX_DIFF ? rawDiff + MAX_ANGLE : rawDiff);
        }

        public Boolean IsTurnCounterCW(IDirection targetDirection)
        {
            double diff = targetDirection.Direction - this.Direction;
            return diff > 0 ? diff <= MAX_DIFF : diff < -MAX_DIFF;
        }

        public override bool Equals(object obj)
        {
            var impl = obj as DirectionImpl;
            return impl != null &&
                   Direction == impl.Direction;
        }

        public override int GetHashCode()
        {
            var hashCode = -111489170;
            hashCode = hashCode * -1521134295 + Direction.GetHashCode();
            return hashCode;
        }

        public override string ToString()
        {
            return "Direction degrees: " + this.Direction + " Direction radians: " + this.GetAsRadians();
        }
    }
}
