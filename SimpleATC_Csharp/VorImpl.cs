﻿using System;
using System.Collections.Generic;

namespace SimpleATC_Csharp
{
    class VorImpl : AbstractRadarElement, IVor
    {
        public string VorId { get; set; }

        public VorImpl(string vorId, IRadarPosition vorPosition) : base(vorPosition)
        {
            this.VorId = vorId;
        }

        public override string ToString()
        {
            return "Vor id: " + this.VorId + " " + this.ElementPosition + "\n\t";
        }

        public override bool Equals(object obj)
        {
            var impl = obj as VorImpl;
            return impl != null &&
                   VorId == impl.VorId;
        }

        public override int GetHashCode()
        {
            return 1981271461 + EqualityComparer<string>.Default.GetHashCode(VorId);
        }
    }
}
