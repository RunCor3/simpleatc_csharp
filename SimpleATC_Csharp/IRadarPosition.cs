﻿using System;

namespace SimpleATC_Csharp
{
    interface IRadarPosition
    {
        IPosition2D ElementPosition { get; }

        IRadarPosition SumPosition(IPosition2D positionOffset);

        Boolean IsWithinRadar();

        IDirection ComputeDirectionToTargetPosition(IRadarPosition targetPosition);

        double DistanceFrom(IRadarPosition position);
    }
}
