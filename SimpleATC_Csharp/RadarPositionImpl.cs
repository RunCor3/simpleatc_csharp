﻿using System;
using System.Collections.Generic;

namespace SimpleATC_Csharp
{
    class RadarPositionImpl : IRadarPosition
    {
        private const double X_BOUND = 30000.0;
        private const double Y_BOUND = 20000.0;
        private readonly object synchronizedLock = new object();
        public IPosition2D ElementPosition { get; private set; }

        public RadarPositionImpl(IPosition2D initialPosition)
        {
            this.ElementPosition = initialPosition;
        }

        public static KeyValuePair<double, double> GetRadarBounds()
        {
            return new KeyValuePair<double, double>(X_BOUND, Y_BOUND);
        }

        public IRadarPosition SumPosition(IPosition2D offsetPosition)
        {
            IPosition2D finalPosition = this.ElementPosition;
            finalPosition.AddX(offsetPosition.X);
            finalPosition.AddY(offsetPosition.Y);

            return new RadarPositionImpl(finalPosition);
        }

        public Boolean IsWithinRadar()
        {
            lock (synchronizedLock)
            {
                return ((Math.Abs(this.ElementPosition.X) <= X_BOUND) &&
                        (Math.Abs(this.ElementPosition.Y) <= Y_BOUND));
            }
            
        }

        public IDirection ComputeDirectionToTargetPosition(IRadarPosition targetPosition)
        {
            double xRelative = targetPosition.ElementPosition.X - this.ElementPosition.X;
            double yRelative = targetPosition.ElementPosition.Y - this.ElementPosition.Y;
            double degrees = ((180 / Math.PI) * (Math.Atan2(yRelative, xRelative)));
            degrees = degrees < 0 ? 360 + degrees : degrees;

            return new DirectionImpl(degrees);
        }

        public double DistanceFrom(IRadarPosition position)
        {
            IPosition2D targetPosition = position.ElementPosition;
            return Math.Sqrt(Math.Pow(targetPosition.X - this.ElementPosition.X, 2)
                    + Math.Pow(targetPosition.Y - this.ElementPosition.Y, 2));
        }

        public override string ToString()
        {
            return "Element Position: " + ElementPosition.ToString();
        }
    }
}
