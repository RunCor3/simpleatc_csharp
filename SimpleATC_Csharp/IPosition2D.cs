public interface IPosition2D
{
    double X { get; }
    double Y { get; }

    void AddX(double xOffset);

    void AddY(double yOffset);
}