﻿using System;

namespace SimpleATC_Csharp
{
    interface IDirection
    {
        double Direction { get; }

        void Sum(IDirection direction);

        void Subtract(IDirection direction);

        double GetAsRadians();

        double CompareTo(IDirection direction);

        Boolean IsTurnCounterCW(IDirection targetDirection);
    }
}
