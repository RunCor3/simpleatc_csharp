﻿using System;

namespace SimpleATC_Csharp
{
    interface IVor : IRadarElement
    {
        string  VorId { get; }
    }
}
